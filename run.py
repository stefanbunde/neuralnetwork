import nn


mynet = nn.SearchNet("db.sqlite")

mynet.make_tables()

wWord, wRiver, wBank = 101, 102, 103
wAxel, wNoack = 105, 106
uWordBank, uRiver, uEarth = 201, 202, 203
uStefanBunde, uTobiasThurow, uAxelNoack, uTeamBB8 = 205, 206, 207, 208


query1 = [wWord, wBank]
query2 = [wAxel, wNoack]

result = [uWordBank, uRiver, uEarth, uStefanBunde, uTobiasThurow, uAxelNoack, uTeamBB8]

mynet.train_query(query1, result, uWordBank)
print mynet.get_result(query1, result)

mynet.train_query(query1, result, uWordBank)
print mynet.get_result(query1, result)

print
print

mynet.train_query(query2, result, uAxelNoack)
print mynet.get_result(query2, result)
mynet.train_query(query2, result, uTeamBB8)
print mynet.get_result(query2, result)
mynet.train_query(query2, result, uAxelNoack)
print mynet.get_result(query2, result)

print mynet.get_result(query1, result)
